#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#export PATH="$PATH:~/scripts"

#alias ls='ls --color=auto'
#alias grep='grep --colour=auto'
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
#alias la='exa -a --color=always --group-directories-first'  # all files and dirs
#alias ll='exa -l --color=always --group-directories-first'  # long format
#alias lt='exa -aT --color=always --group-directories-first' # tree listing
#alias df='df -h'

PS1='[\u@\h \W]\$ '
#alias config='/usr/bin/git --git-dir=/home/ed/dotfiles/ --work-tree=/home/ed'
#alias dgit='/usr/bin/git --git-dir /home/ed/dotfiles/.git --work-tree=/home/ed'
